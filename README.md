This is a mongo db Reactive programming crud operations project

Sample Json :  
   {

        "name": "Mouse",
        "qty": 5,
        "price": 2000.0
    }

i have implemented below operations and its api request as given below

Save Product: http://localhost:8080/products/

Get Product by id : http://localhost:8080/products/{id}

Get all Products : http://localhost:8080/products/

Get a product range : http://localhost:8080/products/product-range?min=5000&max=30000

Update product : http://localhost:8080/products/update/{id}

delete a product : http://localhost:8080/products/delete/{id}


mongo db commands 

to check database:
show dbs

to select a database:
use productsdb

to see collections of database:
show collections

to check collection data:
db.product.find()
